class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string :names
      t.string :last_names
      t.attachment :image
      t.integer :ringtone_id

      t.timestamps
    end
  end
end
