class CreateRingtones < ActiveRecord::Migration[5.0]
  def change
    create_table :ringtones do |t|
      t.string :name
      t.attachment :file

      t.timestamps
    end
  end
end
