require 'test_helper'

class RingtonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ringtone = ringtones(:one)
  end

  test "should get index" do
    get ringtones_url
    assert_response :success
  end

  test "should get new" do
    get new_ringtone_url
    assert_response :success
  end

  test "should create ringtone" do
    assert_difference('Ringtone.count') do
      post ringtones_url, params: { ringtone: { file: @ringtone.file, name: @ringtone.name } }
    end

    assert_redirected_to ringtone_url(Ringtone.last)
  end

  test "should show ringtone" do
    get ringtone_url(@ringtone)
    assert_response :success
  end

  test "should get edit" do
    get edit_ringtone_url(@ringtone)
    assert_response :success
  end

  test "should update ringtone" do
    patch ringtone_url(@ringtone), params: { ringtone: { file: @ringtone.file, name: @ringtone.name } }
    assert_redirected_to ringtone_url(@ringtone)
  end

  test "should destroy ringtone" do
    assert_difference('Ringtone.count', -1) do
      delete ringtone_url(@ringtone)
    end

    assert_redirected_to ringtones_url
  end
end
