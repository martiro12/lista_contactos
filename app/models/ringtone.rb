class Ringtone < ApplicationRecord
	
	has_many 	:contacts

	belongs_to 	:ringtone
end
