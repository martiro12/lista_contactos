class Contact < ApplicationRecord

	has_many 	:notes
	has_many 	:phones
	has_many 	:emails
	has_many 	:contact_groups

	belongs_to 	:ringtone

  	# Configuracion/reglas para la imagen de un contacto
  	has_attached_file :image,
  	:styles => {:thumb => "100x100", :small  => "150x150",
  	 			:medium => "400x400", :large => "600x600"},
    :path => ":rails_root/public/contacts/:id/:style/:basename.:extension",
    :url  => "/contacts/:id/:style/:basename.:extension"

	validates_attachment_size :image, :less_than => 5.megabytes,  :message   => "La imagen debe tener un tamaño máximo de 5 Megabytes (Mb)"
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	validates_attachment_file_name :image, matches: [/png\Z/,/PNG\Z/,/jpe?g\Z/,/JPE?G\Z/,/jpg\Z/,/JPG\Z/]
end
