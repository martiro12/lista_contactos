json.extract! ringtone, :id, :name, :file, :created_at, :updated_at
json.url ringtone_url(ringtone, format: :json)
