json.extract! contact_group, :id, :contact_id, :group_id, :created_at, :updated_at
json.url contact_group_url(contact_group, format: :json)
