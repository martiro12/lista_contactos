Rails.application.routes.draw do
  resources :contact_groups
  resources :groups
  resources :emails
  resources :phones
  resources :notes
  resources :contacts
  resources :ringtones
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
